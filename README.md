# Setup Laravel and Angular Admin 

Follow this instruction to setup Laravel and Angular project with JWT token auth

## Setup Laravel 

```bash
cd api
composer install
php artisian key:generate
php artisan migrate
php artisan db:seed
php artisan serve
```

## Setup Angular

```bash
cd frontend
ng build
ng serve
```

